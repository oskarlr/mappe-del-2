package edu.ntnu.idatt2001;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PatientRegisterTest {
    static Patient patient;

    @BeforeAll
    static void createPatient(){
        patient = new Patient("Tor","Hagen","19892732899","Karl","Sick");
    }

    @Test
    void addPatient() {
        PatientRegister.addPatient(patient);
        assertEquals(PatientRegister.getPatientRegister().size(), 1);
    }

    @Test
    void removePatient() {
        PatientRegister.addPatient(patient);
        PatientRegister.removePatient(patient);
        assertEquals(PatientRegister.getPatientRegister().size(), 0);
    }

}