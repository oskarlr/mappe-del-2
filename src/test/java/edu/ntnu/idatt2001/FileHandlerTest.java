package edu.ntnu.idatt2001;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

class FileHandlerTest {

    @Test
    void load() throws IOException {
        FileHandler.load("src//main//resources//Patients.csv");
        assertEquals("29104300764", PatientRegister.getPatientRegister().get(1).getSocialSecurityNumber().trim());
    }

    @Test
    void save() throws IOException {
        FileHandler.save("Patients.csv");
        assertTrue(Files.exists(Path.of("Patients.csv")));
    }
}