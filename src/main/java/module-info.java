module todo.list.application {
    requires javafx.fxml;
    requires javafx.graphics;
    requires javafx.controls;
    requires java.desktop;

    opens edu.ntnu.idatt2001.controllers to javafx.fxml;

    exports edu.ntnu.idatt2001;
}
