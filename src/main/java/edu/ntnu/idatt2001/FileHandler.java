package edu.ntnu.idatt2001;

import java.io.*;

/**
 *Class for serializing and deserializing PatientRegister
 *
 * @author Oskar Løvstad Remvang
 * @version 1.0.0 2021.04.27
 * @since 2021.04.27
 */
public class FileHandler {

    /**
     * Method for importing a patientregister from a csv file
     * @param filePath
     * @throws IOException
     */
    public static void load(String filePath) throws IOException {
        PatientRegister.getPatientRegister().clear();
        BufferedReader csvReader = new BufferedReader(new FileReader(filePath));
        String row;
        while((row = csvReader.readLine()) != null){
            String[] data = row.split(";");
            Patient patient = new Patient(data[0],data[1],data[3],data[2],"");
            PatientRegister.addPatient(patient);
        }
        csvReader.close();
    }

    /**
     * Method for saving patientregister to a csv file
     * @param filePath
     * @throws IOException
     */
    public static void save(String filePath) throws IOException {
        BufferedWriter csvWriter = new BufferedWriter(new FileWriter(filePath));
        for(Patient patient : PatientRegister.getPatientRegister()){
            csvWriter.write(patient.getFirstname()+";"+patient.getLastname()+";"+patient.getGeneralPractitioner()+
                    ";"+patient.getSocialSecurityNumber()+"\n");
        }
        csvWriter.close();
    }

}
