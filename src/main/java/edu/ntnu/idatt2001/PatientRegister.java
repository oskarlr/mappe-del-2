package edu.ntnu.idatt2001;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *Class for managing the Patient Register (ObservableList<Patient>)
 *
 * @author Oskar Løvstad Remvang
 * @version 1.0.0 2021.04.27
 * @since 2021.04.26
 */
public class PatientRegister {

    private final static ObservableList<Patient> register = FXCollections.observableArrayList();


    /**
     * @return Returns the register
     */
    public static ObservableList<Patient> getPatientRegister(){
        return register;
    }

    /**
     * Adds a Patient unless its already in the register
     * @param patient
     * @return
     */
    public static boolean addPatient(Patient patient){
        if(register.contains(patient)) return false;
        register.add(patient);
        return true;
    }

    /**
     * Removes a Patient unless it does not exist in the register
     * @param patient
     */
    public static void removePatient(Patient patient){
        if(!register.contains(patient)) return;
        register.remove(patient);
    }

}
