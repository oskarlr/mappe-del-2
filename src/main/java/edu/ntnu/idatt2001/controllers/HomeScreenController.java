package edu.ntnu.idatt2001.controllers;

import edu.ntnu.idatt2001.FileHandler;
import edu.ntnu.idatt2001.Patient;
import edu.ntnu.idatt2001.PatientRegister;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

/**
 *Controller class for HomeScreen view
 *
 * @author Oskar Løvstad Remvang
 * @version 1.0.0 2021.04.27
 * @since 2021.04.26
 */
public class HomeScreenController {

    @FXML
    public TableView<Patient> table;
    public TableColumn<Patient, String> firstnameColumn, lastnameColumn, socialSecurityNumberColumn,
            generalPractitionerColumn,diagnosisColumn;
    public Button addButton, deleteButton, editButton;
    public Text status;
    public VBox vBox;

    private static ObservableList<Patient> patientList = PatientRegister.getPatientRegister();

    /**
     * Method for initialzing the Home Screen window
     * @throws Exception
     */
    public void initialize() throws Exception {
        /**
         * Adds a listener to the ObservableList so it updates on change
         */
        patientList.addListener((InvalidationListener) p -> {});

        addButton.setGraphic(new ImageView(new Image("addUser.png")));
        deleteButton.setGraphic(new ImageView(new Image("deleteUser.png")));
        editButton.setGraphic(new ImageView(new Image("editUser.png")));

        /**
         * Adds the different parameters to their correct column in the table
         */
        firstnameColumn.setCellValueFactory(new PropertyValueFactory<>("firstname"));
        lastnameColumn.setCellValueFactory(new PropertyValueFactory<>("lastname"));
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        generalPractitionerColumn.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
        diagnosisColumn.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));
        table.setItems(patientList);
        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    /**
     * Loads a stage by its given fxml and sets its specified title.
     * @param event
     * @param fxmlFile
     * @param title
     * @throws IOException
     */
    private void loadStage(ActionEvent event, String fxmlFile,String title) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource(fxmlFile));
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle(title);
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }

    /**
     * Method for the add button in the home screen
     * @param event
     * @throws IOException
     */
    public void addButton(ActionEvent event) throws IOException {
        loadStage(event,"/AddPatientFXML.fxml","Adding Patient");
    }

    /**
     * Method for the delete button in the home screen
     * @param event
     * @throws IOException
     */
    public void deleteButton(ActionEvent event) throws IOException {

        Patient patient = table.getSelectionModel().getSelectedItem();

        /**
         * Makes sure a patient is selected
         */
        if(patient == null){
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("No Patient Selected");
                alert.setHeaderText("No Patient Selected For Deletion");
                alert.setContentText("Select a patient to remove from the patient register");
                alert.showAndWait();
                return;
            }

        /**
         * Alerts user before deleting a patient
         */
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Deleting Patient");
        alert.setHeaderText("Deleting Patient From Register");
        alert.setContentText("Are you sure you want to delete\n the patient from the register?\n\n" +
                            "Patient: " + patient.getFirstname() + " " + patient.getLastname() + " " +
                            patient.getSocialSecurityNumber());

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            PatientRegister.removePatient(patient);
            status.setText("Status: Patient deleted");
        }

    }

    /**
     * Method for the edit button in home screen
     * @param event
     * @throws IOException
     */
    public void editButton(ActionEvent event) throws IOException {

        Patient patient = table.getSelectionModel().getSelectedItem();

        /**
         * Makes sure a patient is selected
         */
        if(patient == null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("No Patient Selected");
            alert.setHeaderText("No Patient Selected For Editing");
            alert.setContentText("Select a patient to edit");
            alert.showAndWait();
            return;
        }

        EditPatientController.setPatient(patient);
        loadStage(event,"/EditPatientFXML.fxml","Editing Patient");
    }

    /**
     * Dialog for the about MenuItem in the Help Menu
     * @param event
     */
    public void about(ActionEvent event){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog - About");
        alert.setHeaderText("Patient Register\nv0.1-SNAPSHOT");
        alert.setContentText("Simple application created by\n(C)Oskar Remvang\n2021-04-25");
        alert.showAndWait();
    }

    /**
     * Method for importing .cvs files
     * @param event
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void importCSV(ActionEvent event) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Pick a .csv file to import");

        File file = fileChooser.showOpenDialog(vBox.getScene().getWindow());
        if(file==null)return;

        //Gets the extension of the file
        String fileName = file.getName();
        String extension = "";
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            extension = fileName.substring(i+1);
        }
        //Checks that the extension is ".csv"
        if(!extension.equals("csv")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Unsupported filetype");
            alert.setHeaderText("Wrong filetype on selected file");
            alert.setContentText("The chosen file type is not valid.\nPlease choose another file by clicking on Select File or cancel the operation.");
            alert.showAndWait();
            status.setText("Status: Import unsuccessful!");
            return;
        }

        FileHandler.load(file.getAbsolutePath());
        status.setText("Status: Import successful!");
    }

    /**
     * Method for exporting the patient register as a cvs file
     * @param event
     * @throws IOException
     */
    public void exportCSV(ActionEvent event) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select a location for you're .csv file");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV Files",".csv"));
        File file = fileChooser.showSaveDialog(vBox.getScene().getWindow());
        if(file==null) return;

        String fileName = file.getName();

        //Gets the extension of the file
        String extension = "";
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            extension = fileName.substring(i+1);
        }
        //Checks that the extension is ".csv"
        if(!extension.equals("csv")){
            Files.delete(Path.of(file.getAbsolutePath()));
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Unsupported filetype");
            alert.setHeaderText("Wrong filetype on selected file");
            alert.setContentText("The chosen file type is not valid.\nPlease choose another file by clicking on Select File or cancel the operation.");
            alert.showAndWait();
            status.setText("Status: Export unsuccessful");
            return;
        }

        FileHandler.save(file.getAbsolutePath());
        status.setText("Status: Export successful");
    }

    /**
     * Method for the exit MenuItem in the File Menu
     * @param event
     */
    public void exit(ActionEvent event){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Exit");
        alert.setHeaderText("Exiting Application");
        alert.setContentText("Are you sure you want to exit the application?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            Platform.exit();
        }
    }
}
