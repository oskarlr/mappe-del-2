package edu.ntnu.idatt2001.controllers;

import edu.ntnu.idatt2001.Patient;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 *Controller for the Edit Patient view
 *
 * @author Oskar Løvstad Remvang
 * @version 1.0.0 2021.04.27
 * @since 2021.04.26
 */
public class EditPatientController {

    @FXML
    public Button okButton, cancelButton;
    public TextField firstnameField, lastnameField, generalPractitionerField, diagnosisField;

    private static Patient patient;

    /**
     * Method for initializing the Edit Patient window
     * @throws Exception
     */
    public void initialize() throws Exception {
        firstnameField.setText(patient.getFirstname());
        lastnameField.setText(patient.getLastname());
        generalPractitionerField.setText(patient.getGeneralPractitioner());
        diagnosisField.setText(patient.getDiagnosis());
    }

    /**
     * Sets the Patient to be edited
     * @param patient
     */
    public static void setPatient(Patient patient) {
        EditPatientController.patient = patient;
    }

    /**
     * Method for the ok button
     * @param event
     */
    public void okButton(ActionEvent event) {

        //Throws a warning if fields firstname or lastname are empty
        if(firstnameField.getText().trim().equals("") || lastnameField.getText().trim().equals("")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Empty Fields");
            alert.setContentText("Firstname and lastname cannot be left empty");

            alert.showAndWait();
            return;
        }

        //Sets fields of the Patient being edited to the values from the TextFields
        patient.setFirstname(firstnameField.getText());
        patient.setLastname(lastnameField.getText());
        patient.setGeneralPractitioner(generalPractitionerField.getText());
        patient.setDiagnosis(diagnosisField.getText());

        (((Node)event.getSource()).getScene().getWindow()).hide();
    }

    /**
     * Method for the cancel button
     * @param event
     */
    public void cancelButton(ActionEvent event) {
        (((Node)event.getSource()).getScene().getWindow()).hide();
    }


}
