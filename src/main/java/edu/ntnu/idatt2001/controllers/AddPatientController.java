package edu.ntnu.idatt2001.controllers;

import edu.ntnu.idatt2001.Patient;
import edu.ntnu.idatt2001.PatientRegister;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 *Controller for the Add Patient view
 *
 * @author Oskar Løvstad Remvang
 * @version 1.0.0 2021.04.27
 * @since 2021.04.26
 */
public class AddPatientController {

    @FXML
    public Button okButton, cancelButton;
    public TextField firstnameField, lastnameField, socialSecurityNumberField, generalPractitionerField, diagnosisField;

    /**
     * Method for initializing the Add Patient window
     * @throws Exception
     */
    public void initialize() throws Exception {
        //Sets propmt texts for all input fields
        firstnameField.setPromptText("Firstname");
        lastnameField.setPromptText("Lastname");
        socialSecurityNumberField.setPromptText("Social Security Number");
        generalPractitionerField.setPromptText("General Practitioner");
        diagnosisField.setPromptText("Diagnosis");
    }

    /**
     * Method for Add Patients ok button.
     * @param event
     */
    public void okButton(ActionEvent event) {

        Alert alert = new Alert(Alert.AlertType.WARNING);

        //Throws a warning if fields firstname, lastname, or social security number are empty
        if(firstnameField.getText().trim().equals("") || lastnameField.getText().trim().equals("") || socialSecurityNumberField.getText().trim().equals("")){
            alert.setTitle("Warning");
            alert.setHeaderText("Empty Fields");
            alert.setContentText("The firstname, lastname or the social security number\n cannot be left empty");

            alert.showAndWait();
            return;
        }

        try{
            //Throws a warning if the social security number input cannot be parsed to a Long
            Long.parseLong(socialSecurityNumberField.getText());
        }catch (Exception e){
            alert.setTitle("Warning");
            alert.setHeaderText("Unvalid Social Security Number");
            alert.setContentText("The Social Security Number must be an 11-digit number");

            alert.showAndWait();
            return;
        }

        //Throws a warning if the social security number input is not 11 digits long
        if(socialSecurityNumberField.getText().trim().length()!=11) {
            alert.setTitle("Warning");
            alert.setHeaderText("Unvalid Social Security Number");
            alert.setContentText("The Social Security Number must be an 11-digit number");

            alert.showAndWait();
            return;
        }

        Patient patient = new Patient(
                firstnameField.getText(),
                lastnameField.getText(),
                socialSecurityNumberField.getText(),
                generalPractitionerField.getText(),
                diagnosisField.getText());

        //Checks if the patient is already in the register when being added
        if(!PatientRegister.addPatient(patient)){
            alert.setTitle("Warning");
            alert.setHeaderText("Patient already in register");
            alert.setContentText("The patient is already in the register,\nplease try a new patient");

            alert.showAndWait();
            return;
        }

        ((Node)event.getSource()).getScene().getWindow().hide();
    }

    /**
     * Method for Add Patients cancel button
     * @param event
     */
    public void cancelButton(ActionEvent event) {
        ((Node)event.getSource()).getScene().getWindow().hide();
    }

}
