package edu.ntnu.idatt2001;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *Class representing a Patient
 *
 * @author Oskar Løvstad Remvang
 * @version 1.0.0 2021.04.27
 * @since 2021.04.26
 */
public class Patient {

    private StringProperty firstname = new SimpleStringProperty();
    private StringProperty lastname = new SimpleStringProperty();
    private StringProperty socialSecurityNumber = new SimpleStringProperty();
    private StringProperty generalPractitioner = new SimpleStringProperty();
    private StringProperty diagnosis = new SimpleStringProperty();

    public Patient(String firstname, String lastname, String socialSecurityNumber, String generalPractitioner, String diagnosis) {
        this.firstname.set(firstname);
        this.lastname.set(lastname);
        this.socialSecurityNumber.set(socialSecurityNumber);
        this.generalPractitioner.set(generalPractitioner);
        this.diagnosis.set(diagnosis);
    }

    public final String getFirstname(){
        return firstname.get();
    }

    public final void setFirstname(String name) {
        firstname.set(name);
    }

    public final String getLastname(){
        return lastname.get();
    }

    public final void setLastname(String name) {
        lastname.set(name);
    }

    public final String getSocialSecurityNumber() {
        return socialSecurityNumber.get();
    }

    public final String getGeneralPractitioner(){
        return generalPractitioner.get();
    }

    public final void setGeneralPractitioner(String gp) {
        generalPractitioner.set(gp);
    }

    public final String getDiagnosis() {
        return diagnosis.get();
    }

    public final void setDiagnosis(String dg) {
        diagnosis.set(dg);
    }

    @Override
    public String toString() {
        return "Patient{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", socialSecurityNumber=" + socialSecurityNumber +
                ", generalPractitioner='" + generalPractitioner + '\'' +
                ", diagnosis='" + diagnosis + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return socialSecurityNumber == patient.socialSecurityNumber;
    }

}
