package edu.ntnu.idatt2001;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;


/**
 *Main class for running the javafx application
 *
 * @author Oskar Løvstad Remvang
 * @version 1.0.0 2021.04.27
 * @since 2021.04.26
 */
public class Main extends Application {

    public static void main(String[] args) {
        fillWithTestData();
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/HomeScreenFXML.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setTitle("Patient Register Application");

        Image image = new Image("hospital.png");
        stage.getIcons().add(image);

        stage.show();
    }

    /**
     * Method used for testing. Fills register with patients.
     */
    private static void fillWithTestData(){
        Patient are = new Patient("Are","Ketil","12312414121","Mari","Sick");
        Patient tor = new Patient("Tor","Larsen","98268977388","Hari","Too Cool");
        Patient bjørg = new Patient("Bjørg","Leifsen","67890876545","Bari","Long Foot");
        Patient karl = new Patient("Karl","Karlssen","67893453453","Sari","Jittery");
        PatientRegister.addPatient(are);
        PatientRegister.addPatient(tor);
        PatientRegister.addPatient(bjørg);
        PatientRegister.addPatient(karl);
    }
}
