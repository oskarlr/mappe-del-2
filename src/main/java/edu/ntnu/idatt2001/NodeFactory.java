package edu.ntnu.idatt2001;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 *Factory class for creating JavaFX Nodes
 *
 * @author Oskar Løvstad Remvang
 * @version 1.0.0 2021.04.27
 * @since 2021.04.27
 */
public class NodeFactory {

    /**
     * Factory method
     * @param nodeType
     * @return Node according to nodeType
     */
    public static Node getNode(String nodeType,double height, double width){
        switch (nodeType){
            case "MENUBAR":
                MenuBar menuBar = new MenuBar();
                menuBar.setLayoutX(width);
                menuBar.setLayoutY(height);
                return menuBar;
            case "TOOLBAR":
                ToolBar toolBar = new ToolBar();
                toolBar.setLayoutX(width);
                toolBar.setLayoutY(height);
                return toolBar;
            case "BORDERPANE":
                BorderPane borderPane = new BorderPane();
                borderPane.setLayoutX(width);
                borderPane.setLayoutY(height);
                return borderPane;
            case "VBOX":
                VBox vBox = new VBox();
                vBox.setLayoutX(width);
                vBox.setLayoutY(height);
                return vBox;
            case "HBOX":
                HBox hBox = new HBox();
                hBox.setLayoutX(width);
                hBox.setLayoutY(height);
                return hBox;
            case "TABLEVIEW":
                TableView<Patient> tableView = new TableView<>();
                tableView.setLayoutX(width);
                tableView.setLayoutY(height);
                return tableView;
            case "BUTTON":
                Button button = new Button();
                button.setLayoutX(width);
                button.setLayoutY(height);
                return button;
            case "TEXT":
                Text text = new Text();
                text.setLayoutX(width);
                text.setLayoutY(height);
                return text;
            default:
                return null;
        }
    }

}
